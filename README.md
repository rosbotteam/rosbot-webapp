# README #

This repo contains the Django server used in the RosBot project.

### Quick Overview ###

For more information, visit the [Django homepage](https://www.djangoproject.com/).

* './rosbot' contains the application information of this Django server. It holds the admin, models, urls, and views Python files that are used when navigating to this application's page on the server. It also holds the Django migration information in './migrations' and the static files for the application in './static/rosbot'.
* './rosbot_webapp' contains the Django project files. It holds the settings and urls Python files for the entire Django project.
* './templates' contains the webpage templates for this Django project.