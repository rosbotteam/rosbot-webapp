from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.rosbot, name='rosbot'),
    url(r'^command/(.+)/', views.command, name='command')
]
