from django.shortcuts import render
from django.http import HttpRequest, HttpResponse

import rosbot_comm

def rosbot(request):
    return render(request, 'rosbot/rosbot.html', {})

def command(request, direction):
    if direction == "forward":
        rosbot_comm.move_forward(["192.168.101.10", 6969])
    elif direction == "backward":
        rosbot_comm.move_backward(["192.168.101.10", 6969])
    elif direction == "left":
        rosbot_comm.turn_left(["192.168.101.10", 6969])
    elif direction == "right":
        rosbot_comm.turn_right(["192.168.101.10", 6969])
    return HttpResponse()
