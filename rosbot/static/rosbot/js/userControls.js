UserControl = {
  docInit:function(){
    function GetChar (event){
      var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
      alert ("The Unicode character code is: " + chCode);
    }

    var mouseDownOnButton = false;
    var xmlHttp = new XMLHttpRequest();
    var btnup = 'command/forward'
    var btnleft = 'command/left'
    var btnright = 'command/right'
    var btndwn = 'command/backward'

    var wKeyCode = 87;
    var aKeyCode = 65;
    var sKeyCode = 83;
    var dKeyCode = 68;

    //BIND KEYBOARD
    $(document).bind('keydown',function(e){
      if(e.keyCode == 87) {
        xmlHttp.open( "GET", btnup, false ); xmlHttp.send( null );
      }
    });

    $(document).bind('keydown',function(e){
      if(e.keyCode == 65) {
        xmlHttp.open( "GET", btnleft, false ); xmlHttp.send( null );
      }
    });

    $(document).bind('keydown',function(e){
      if(e.keyCode == 83) {
        xmlHttp.open( "GET", btndwn, false ); xmlHttp.send( null );
      }
    });

    $(document).bind('keydown',function(e){
      if(e.keyCode == 68) {
        xmlHttp.open( "GET", btnright, false ); xmlHttp.send( null );
      }
    });


    //BIND MOUSE HOLDS
    $("#blef").mousedown(function() {
      mouseDownOnButton = true;
      var internvalId = ssetInterval(function(){ 
        if(mouseDownOnButton){
          setTimeout(function(){ xmlHttp.open( "GET", btnleft, false ); xmlHttp.send( null ); console.log('blef clicked')}, 100);
        }
        else{
          clearInterval(internvalId);
        }
      }, 100)
    });

    $("#bup").mousedown(function() {
      mouseDownOnButton = true;
      var internvalId = setInterval(function(){ 
        if(mouseDownOnButton){
          setTimeout(function(){ xmlHttp.open( "GET", btnup, false ); xmlHttp.send( null ); console.log('bup clicked')}, 100);
        }
        else{
          clearInterval(internvalId);
        }
      }, 100)
    });

    $("#bdwn").mousedown(function() {
      mouseDownOnButton = true;
      var internvalId = setInterval(function(){ 
        if(mouseDownOnButton){
          setTimeout(function(){ xmlHttp.open( "GET", btndwn, false ); xmlHttp.send( null ); console.log('bdwn clicked')}, 100);
        }
        else{
          clearInterval(internvalId);
        }
      }, 100)
    });

    $("#brgt").mousedown(function() {
      mouseDownOnButton = true;
      var internvalId = setInterval(function(){ 
        if(mouseDownOnButton){
          setTimeout(function(){ xmlHttp.open( "GET", btnright, false ); xmlHttp.send( null ); console.log('brgt clicked')}, 100);
        } 
        else{
          clearInterval(internvalId);
        }
      }, 100);
    });


    $(document).mouseup(function() {
      if(mouseDownOnButton){
        mouseDownOnButton = false;
      }
    });
    
    console.log('UserControls, document initialization completed');

  },

}